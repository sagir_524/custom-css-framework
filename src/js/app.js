import $ from 'jquery'

/**
 * preloader
 */
let $preloader = $('.preloader');

const addLoader    = () => $preloader.stop().fadeIn(500)
const removeLoader = () => $preloader.stop().fadeOut(500)

$(window).on('load', () => {
  setTimeout(removeLoader, 1000)
})

/**
 * off-canvas--backdrop
 */
let $backdrop = $('.backdrop')

const addBackdrop = ()    => $backdrop.stop().fadeIn(400)
const removeBackdrop = () => $backdrop.stop().fadeOut(400)

/**
 * mobile off-canvas menu
 */
let $sidebar = $('.sidebar')

$('.menu-icon').on('click', (event) => {
  event.preventDefault()
  
  $sidebar.addClass('active')
  addBackdrop()
})

$backdrop.on('click', () => {
  $sidebar.removeClass('active')
  removeBackdrop()
})