# custom-css-framework

A project specific CSS framework

### How to run this project

First clone this project.
Make sure you have node.js installed.
Open you terminal/command-line on project folder.
Then run:

```
npm install
npm run dev
```

Then goto http://localhost:1234
